﻿using System;
using CampoSubmarino;

namespace JuegoSubmarino
{
    class Program 
    {
        static void Main()
        {
            Console.CursorVisible = false;
            
            // Objeto de clase Juego
            Juego _juego = new Juego();
            
            // Variable booleana para ciclo DO
            bool continuar = false;
            
            // Variables relacionadas a la pantalla y jugador
            
            Console.WriteLine("" +
                              "__   __   _ _                           _                          _            \n" +
                              "\\ \\ / /  | | |                         | |                        (_)\n" +
                              " \\ V /___| | | _____      __  ___ _   _| |__  _ __ ___   __ _ _ __ _ _ __   ___ \n" +
                              "  \\ // _ \\ | |/ _ \\ \\ /\\ / / / __| | | | '_ \\| '_ ` _ \\ / _` | '__| | '_ \\ / _ \\\n" +
                              "  | | __ / | | (_) \\ V  V /  \\__ \\ |_| | |_) | | | | | | (_| | |  | | | | |  __/\n" +
                              "  \\_/\\___|_|_|\\___/ \\_/\\_/   |___/\\__,_|_.__/|_| |_| |_|\\__,_|_|  |_|_| |_|\\___|\n");
            Console.WriteLine("  ~~~~~~~~~~~~~~~~~~~~~~~~~~~|~~~~~~~~~~~~~~~~~~~~~~~~~o~~~~~~~~~~~\n" +
                              "            o                |               o      __o\n" +
                              "           o                 |               o   | X__ >\n" +
                              "       ___o                  |            __o\n" +
                              "     (X___ > --           __ | __      | X__ > o\n" +
                              "                          |     \\                    __o\n" +
                              "                          |      \\                | X__ >\n" +
                              "  _______________________ | _______\\________________\n" +
                              " <                                                \\____________   _\n" +
                              "  \\                                                            \\ (_)\n" +
                              "   \\    O       O       O                                        >=)\n" +
                              "    \\__________________________________________________________ / (_)\n");
            Console.SetCursorPosition(20, 20);
            Console.WriteLine("PRESIONA ENTER PARA COMENZAR\n");
            Console.ReadKey();
            
            do
            {
                try
                {
                    Console.Clear();
                    // Menu de opciones
                    Console.WriteLine("Bienvenido al juego del submarino amarillo.");
                    Console.WriteLine("\nMenu de opciones:");
                    Console.WriteLine("\t1. Jugar.");
                    Console.WriteLine("\t2. Reglas del juego.");
                    Console.WriteLine("\t3. Ver puntajes.");
                    Console.WriteLine("\t4. Salir.");
                    var opcion = Convert.ToInt32(Console.ReadLine());

                    switch (opcion)
                    {
                        // Jugar
                        case 1:
                            Console.Clear();
                            _juego.Jugar();
                            break;
                        
                        // Reglas
                        case 2:
                            Console.Clear();
                            _juego.ReglasDelJuego();
                            break;

                        // Puntajes
                        case 3:
                            Console.Clear();
                            _juego.VerPuntajes();
                            break;

                        // Salir
                        case 4:
                            Console.Clear();
                            Console.WriteLine("Fin del juego. Hasta pronto!");
                            continuar = true;
                            Console.ReadKey();
                            break;
                        
                        default:
                            Console.Clear();
                            Console.WriteLine("Opcion no valida.");
                            Console.ReadKey();
                            break;
                    }
                    
                }
                catch (FormatException error)
                {
                    continuar = false;
                    Console.WriteLine("La opcion seleccionada debe ser numerica. \nVolve a intentarlo.");
                    Console.ReadKey();
                }
            } while (!continuar);
        }
    }
}