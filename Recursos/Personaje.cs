using System;
using System.Diagnostics;

namespace Recursos
{
    public class Personaje
    {
        // Atributos y propiedades
        public int X { get; set; }
        public int Y { get; set; }
        public int Yinicial { get; set; }
        public bool Vida { get; set; }
        public int TiempoRestante { get; set; }
        private string Submarino { get; set; }
        public Stopwatch TiempoVida { get; set; } // Cuenta el puntaje obtenido = segundos vivo


        // Constructor personaje
        public Personaje()
        {
            this.X = 5;
            this.Y = 10;
            this.Yinicial = 10;
            this.Vida = true;
            this.TiempoRestante = 160;
            this.Submarino = "S";
            this.TiempoVida = new Stopwatch();
            this.TiempoVida.Start();
        }


        // Movimiento del personaje
        public void MoverPersonaje(ConsoleKeyInfo tecla)
        {
            switch (tecla.Key)
            {
                case ConsoleKey.DownArrow:
                    this.Yinicial = this.Y;
                    if (this.Y <= 19) // Limite inferior del campo de juego
                    {
                        this.Y++;
                    }
                    break;
                
                case ConsoleKey.UpArrow:
                    this.Yinicial = this.Y;
                    if (this.Y >= 2) // Limite superior del campo de juego
                    {
                        this.Y--;
                    }
                    break;

                default:
                    // Queda vacio porque sino tocas up or down no hace nada
                    break;
            }
        }


        // Funcion Dibujar
        public void Dibujar()
        {
            Borrar();
            Console.SetCursorPosition(3, 22);
            Console.Write("                      ");
            Console.SetCursorPosition(3, 22);
            Console.Write("Oxigeno restante: {0}", this.TiempoRestante);
            Console.SetCursorPosition(26, 22);
            Console.Write("Puntaje: {0}", String.Format("{0:00000}", this.TiempoVida.Elapsed.TotalSeconds));
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.SetCursorPosition(this.X, this.Y);
            Console.Write(this.Submarino);
        }


        // Funcion Borrar
        private void Borrar()
        {
            Console.SetCursorPosition(this.X, this.Yinicial);
            Console.Write(" ");
        }
    }
}