using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Recursos
{
    public class Campo
    {
        // Atributos y propiedades
        public List<Punto> ElementosVisibles { get; set; }
        public List<Punto> Elementos { get; set; }
        public float MultiplicadorTiempo { get; set; } // Modifica Velocidad del juego
        public Stopwatch St { get; set; } // Controla tiempo de PowerUp activo
        public bool TriggerPU { get; set; } // Verifica si esta activo el PowerUp
        public bool Acelerado { get; set; } // Verifica si ya se acelero el juego


        // Funcion booleana de colision, se pasa el jugador como parametro
        public bool Colision(Personaje jugador)
        {
            foreach (var p in this.ElementosVisibles) // Verifica si hay colision con los objetos visibles (que son los que estan activos)
            {
                if (p.X == jugador.X && p.Y == jugador.Y)
                {
                    p.X = 0; // Muevo el elemento con el que colisiono a la posicion 0 para que no colisione de nuevo
                    // 0 es mina
                    // 1 es oxigeno
                    // 2 es PowerUp(Realentiza el juego)
                    // 3 es PowerUp(Destruye minas en pantalla)
                    switch (p.Elemento) 
                    {
                        case 0: jugador.Vida = false; // Game over
                            break;
                        case 1: jugador.TiempoRestante += 100; // Al agregar un oxigeno, se suma al contador de oxigeno
                            break;
                        case 2: this.MultiplicadorTiempo -= 0.50f;// Realentiza el juego, menor el numero => mas lento va
                            this.TriggerPU = true;
                            this.St = new Stopwatch();
                            this.St.Start();
                            break;
                        case 3: foreach(Punto punto in this.ElementosVisibles) // PowerUp P, borra todas las minas de la pantalla
                                {
                                    if (punto.Elemento == 0) // Si es una mina
                                    {
                                        punto.X = 0;
                                    }
                                }
                            break;
                        default: break;
                    }
                    return true; // True si hay colision
                }
            }
            return false; // False si no hay colision
        }


        // Funcion inicializar
        public void Inicializar()
        {
            // Atributos y propiedades
            Random _random = new Random(DateTime.Now.Millisecond);
            this.Elementos = new List<Punto>();
            this.ElementosVisibles = new List<Punto>();
            this.MultiplicadorTiempo = 1;
            this.TriggerPU = false;
            this.Acelerado = false;
            Punto _punto = null;

            // Aparicion de minas en pantalla
            for (var i = 0; i < 60; i++) // i es la cantidad de minas
            {
                _punto = new Punto(50, _random.Next(1, 21), 0);
                this.Elementos.Add(_punto);
            }
            // Aparicion de oxigenos en pantalla
            for (var j = 0; j < 3; j++)
            {
                _punto = new Punto(50, _random.Next(1,21),1);
                this.Elementos.Add(_punto);
            }
            // Creacion de PowerUps
            _punto = new Punto(50, _random.Next(1, 21), 2);
            this.Elementos.Add(_punto);
            _punto = new Punto(50, _random.Next(1, 21), 3);
            this.Elementos.Add(_punto);
        }


        // Funcion dibujar
        public void Dibujar()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            foreach (var l in this.ElementosVisibles)
            {
                Console.SetCursorPosition(l.Xprevia, l.Y);
                Console.Write(" ");
                Console.SetCursorPosition(l.X, l.Y);
                Console.Write(l.Caracter);
            }
            // Arreglo para elementos que no se encuentran mas en X = 0 o 1
            for (int j = 0; j < 3; j++) 
            {
                for (int i = 1; i < 21; i++)
                {
                    Console.SetCursorPosition(j, i);
                    Console.Write(" ");
                }
            }
            Console.SetCursorPosition(0, 21);
            Console.WriteLine("=================================================");
        }
    }
}