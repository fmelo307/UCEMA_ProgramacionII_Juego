using System;

namespace Recursos
{
    public class Punto
    {
        // Atributos y propiedades
        public int X { get; set; }
        public int Y { get; set; }
        public int Elemento { get; set; }  // Tipo de elemento: 0 mina y 1 oxigeno
        public int Xprevia { get; set; }
        public string Caracter { get; set; }
        
        
        // Constructor Punto
        public Punto(int a, int b, int c)
        {
            this.X = a;
            this.Y = b;
            this.Elemento = c;
            if (this.Elemento == 0)
            {
                this.Caracter = "*"; // Caracter Mina
            }

            if (this.Elemento == 1)
            {
                this.Caracter = "@"; // Caracter Oxigeno
            }

            if (this.Elemento == 2)
            {
                this.Caracter = "T"; // Caracter PowerUp / Realentiza el "tiempo del juego"
            }

            if (this.Elemento == 3)
            {
                this.Caracter = "P"; // Caracter PowerUp / Borra todas las minas de la pantalla
            }
        }
        
        
        // Funcion movimiento de minas y oxigeno
        public void MovimientoPunto()
        {
            this.Xprevia = this.X; // Se guarda la posicion anterior actualizar el dibujo
            if (this.X > 0)
            {
                this.X--;
            }
        }
    }
}