using System.Diagnostics;
using System.Dynamic;

namespace Recursos
{
    public class Tablero
    {
        // Atributos y propiedades
        public bool GameOver { get; set; } 
        public Personaje _personaje = null;
        public Campo _campo = null;

        
        // Constructor Tablero
        public Tablero()
        {
            this.GameOver = false;
            this._personaje = new Personaje();
            this._campo = new Campo();
        }


        // Funcion Salir
        public void Salir()     // no esta llamada desde ningun lado esta funcion
        {
            this.GameOver = true;    
        }


        // Funcion Colision
        public void Colision()
        {
            _campo.Colision(this._personaje);
        }
    }
}