using System;
using Recursos;

namespace CampoSubmarino
{
    static class Dibujar
    {
        public static void Ejecutar(Tablero _tablero)
        {
            _tablero._campo.Dibujar();
            _tablero._personaje.Dibujar();
            _tablero.Colision();
        }
    }
}