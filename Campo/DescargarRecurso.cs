using System;
using System.Collections.Generic;
using Recursos;

namespace CampoSubmarino
{
    public static class DescargarRecurso
    {
        public static Tablero Ejecutar(Tablero _tablero)
        {
            _tablero._personaje = null;

            
            /*
             * Se vacian los elementos
             */
            for (int ev = _tablero._campo.ElementosVisibles.Count-1; ev >=0 ; ev--)
            {
                _tablero._campo.ElementosVisibles.RemoveAt(ev);
            }
            
            for (int ei = _tablero._campo.Elementos.Count-1; ei >=0 ; ei--)
            {
                _tablero._campo.Elementos.RemoveAt(ei);
            }

            _tablero._campo.Elementos = null;

            _tablero._campo.ElementosVisibles = null;

            _tablero._campo = null;

            _tablero = null;


            return _tablero;
        }
    }
}