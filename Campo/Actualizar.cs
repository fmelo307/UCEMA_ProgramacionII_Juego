using System;
using System.Runtime.CompilerServices;
using Recursos;
using System.Threading;

namespace CampoSubmarino
{
    public static class Actualizar
    {
        public static Tablero Ejecutar(Tablero _tablero)
        {
            // Para generar los (puntos) oxigenos y minas
            Random _random = new Random();

            // OBSOLETA
            // Salir si el jugador gano (no quedan elementos en movimiento visibles en pantalla exceptuando al jugador)
            //if ((_tablero._campo.ElementosVisibles.Count == 62 && _tablero._campo.ElementosVisibles[61].X == 0)) 
            //{
            //    Console.Clear();
            //    Console.ForegroundColor = ConsoleColor.Yellow;
            //    Console.WriteLine("Felicidades! Has sobrevivido con {0} de ox�geno restante", _tablero._personaje.TiempoRestante);
            //    Thread.Sleep(3000);
            //    _tablero.GameOver = true;
            //    return _tablero;
            //}

            // Salir si el jugador perdio, sea porque el oxigeno llego a 0 o porque colisiono con una mina
            if (_tablero._personaje.TiempoRestante == 0 || _tablero._personaje.Vida == false)
            {
                Console.Clear();
                _tablero._personaje.TiempoVida.Stop();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\tGAME OVER");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("\tTu puntaje es {0}", Math.Floor(_tablero._personaje.TiempoVida.Elapsed.TotalSeconds));
                Thread.Sleep(1000);
                _tablero.GameOver = true;
                return _tablero;
            }

            
            // Tecla Up&Down para jugar
            // Escape para salir
            ConsoleKeyInfo teclaMover;
            if (Console.KeyAvailable == true)
            {
                teclaMover = Console.ReadKey(true);
                if (teclaMover.Key == ConsoleKey.Escape)
                {
                    _tablero.GameOver = true;
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("\tTu puntaje es {0}", Math.Floor(_tablero._personaje.TiempoVida.Elapsed.TotalSeconds));
                }
                _tablero._personaje.MoverPersonaje(teclaMover);
            }


            // Pase de puntos de la lista de elementos a la lista de elementos visibles
            if (_random.Next(0, 2) == 0)
            {
                Punto _punto = null;
                if (_tablero._campo.Elementos.Count != 0)
                {
                    _punto = _tablero._campo.Elementos[_random.Next(0, _tablero._campo.Elementos.Count)];
                    _tablero._campo.Elementos.Remove(_punto);
                    _tablero._campo.ElementosVisibles.Add(_punto);
                }
            }

            
            // Movimiento del submarino
            foreach (var ele in _tablero._campo.ElementosVisibles)
            {
                ele.MovimientoPunto();
                if (ele.X == 0)
                {
                    Random r = new Random(DateTime.Now.Millisecond);
                    ele.X = 50;
                    ele.Y = r.Next(1, 20);
                    _tablero._campo.Elementos.Add(ele);
                }
            }

            // Reiniciar elementos
            foreach (var ele in _tablero._campo.Elementos)
            {
                if(_tablero._campo.ElementosVisibles.Contains(ele))
                {
                    _tablero._campo.ElementosVisibles.Remove(ele);
                }
            }

            // Llamada a la funcion Colision en clase Tablero
            _tablero.Colision();

            
            // La funcion se corre cada 1/4 de segundo y va restando 1 milisegundo al tiempo restante
            // Para sumar tiempo, el submarino debe colisionar con un oxigeno
            _tablero._personaje.TiempoRestante--;
            

            // Acelera el juego si el jugador ya ha sobrevivido mas de 20s
            if (_tablero._campo.Acelerado == false && _tablero._personaje.TiempoVida.ElapsedMilliseconds >= 20000) 
            {
                _tablero._campo.MultiplicadorTiempo += 0.55f;
                _tablero._campo.Acelerado = true;
            }


            //Verifica si esta activo el PowerUp de realentizacion y en ese caso controla cuando se expira
            if (_tablero._campo.TriggerPU == true && _tablero._campo.St.ElapsedMilliseconds >= 5000) // Segunda condicion regula duracion de PowerUp 
            {
                _tablero._campo.St.Stop();
                _tablero._campo.St = null;
                _tablero._campo.MultiplicadorTiempo += 0.5f;
                _tablero._campo.TriggerPU = false;
            }
            //Pause para actualizar, controla la velocidad del juego mediante el multiplicador
            Thread.Sleep(250 + (int)Math.Floor(250 * (1 - _tablero._campo.MultiplicadorTiempo))); 


            return _tablero;
        }
    }
}