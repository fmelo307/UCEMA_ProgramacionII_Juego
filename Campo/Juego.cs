using System;
using System.Collections.Generic;
using System.Linq;
using Recursos;


namespace CampoSubmarino
{
    public class Juego
    {
        // Atributos y propiedades
        private Tablero _tablero;
        private Dictionary<string, int> Puntajes { get; set; }


        // Constructor Juego
        public Juego() 
        {
            Puntajes = new Dictionary<string, int>();
        }


        // Funcion Jugar
        public void Jugar()
        {
            this._tablero = CargarRecurso.Ejecutar(this._tablero);
            do
            {
                Dibujar.Ejecutar(this._tablero);
                this._tablero = Actualizar.Ejecutar(this._tablero);
            } while (!this._tablero.GameOver);
            
            CargarPuntaje(int.Parse((Math.Floor(this._tablero._personaje.TiempoVida.Elapsed.TotalSeconds)).ToString())); // Envia el puntaje a la funcion para cargarlos

            this._tablero = DescargarRecurso.Ejecutar(this._tablero);

            Salir.Ejecutar(this._tablero);
        }

        
        // Funcion que muestra las reglas del juego
        public void ReglasDelJuego()
        {
            Console.WriteLine("Reglas del juego:");
            Console.WriteLine("\tEl submarino se representa con el caracter S.");
            Console.WriteLine("\tEl juego consiste en esquivar las minas en el oceano, representadas con el caracter *.");
            Console.WriteLine("\tAdemas, el jugador cuenta con un contador de oxigeno.");
            Console.WriteLine("\tPara incrementar la cantidad de oxigeno, debe colisionar con el oxigeno extre representado por @.");
            Console.WriteLine("\tSi el jugador colisiona con una mina o se queda sin oxigeno, pierde la partida y se termina el juego.");
            Console.WriteLine("\tSi el jugador colisiona con el PowerUp T disminuye la velocidad del juego temporalmente.");
            Console.WriteLine("\tSi el jugador colisiona con el PowerUp P se destruyen todas las minas en la pantalla en ese instante.");
            Console.WriteLine("\n\nENTER para regresar.");
            Console.ReadKey();
        }


        public void VerPuntajes() // Imprime los puntajes obtenidos anteriormente en la sesion con nombres de los jugadores
        {
            Console.SetCursorPosition(10, 1);
            Console.WriteLine("Puntajes");
            Console.SetCursorPosition(10, 2);
            Console.WriteLine("---------");
            if (Puntajes.Count == 0)
            {
                Console.WriteLine("\n\t No hay puntajes aun");
            }
            else 
            {
                List<KeyValuePair<string, int>> ListaTmp = Ordenar(Puntajes);
                int i = 1;
                foreach (KeyValuePair<string, int> valor in ListaTmp)
                {
                    Console.WriteLine("\n\t{0} {1} - {2}", i, valor.Key, valor.Value);
                    i++;
                }
            }
            Console.WriteLine("\n\n\tENTER para regresar.");
            Console.ReadKey();
        }


        private void CargarPuntaje(int puntos) // Recibe los puntos del jugador y los guarda con el nombre que ingresa
        {
            Console.WriteLine("\tIngrese su nombre: ");
            Console.CursorVisible = true;
            Console.Write("\t");
            string nom = Console.ReadLine();
            this.Puntajes.Add(nom, puntos);
            Console.CursorVisible = false;
        }


        private List<KeyValuePair<string, int>> Ordenar(Dictionary<string, int> dic) // Funcion auxiliar que recibe el diccionario con puntajes y nombres => devuelve lista ordenada con los mismo valores
        {
            var lista = dic.ToList();
            lista.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value)); // Ordena de menor a mayor
            lista.Reverse(); // Invierto el orden
            return lista;
        }
    }
}