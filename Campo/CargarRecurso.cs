using System;
using Recursos;

namespace CampoSubmarino
{
    public static class CargarRecurso
    {
        public static Tablero Ejecutar(Tablero _tablero)
        {
            _tablero = new Tablero();
            _tablero._campo.Inicializar();
            return _tablero;
        }
    }
}