using System;
using System.Reflection;
using Recursos;

namespace CampoSubmarino
{
    public static class Salir
    {
        public static void Ejecutar(Tablero _tablero)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Fin del juego.");
            Console.WriteLine("Presiona ENTER para volver al menu");
            Console.ReadKey();
        }
    }
}